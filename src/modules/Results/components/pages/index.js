import React,{useEffect, useState} from 'react';


import  Storage  from 'storage'

import Score from 'modules/Results/components/organisms/Score';


const Results  = () =>{
    const [scores, setScores] = useState([]);

    useEffect(()=>{
      async function getStorageData (){
        const storage =  new Storage().getData();
        setScores(storage);
      }
      getStorageData();
        
    },[]);

    return (
      <div className="game">
        <h1>Recent games:</h1>
        <Score scores={scores} />
      </div>
    )

}
export default Results;