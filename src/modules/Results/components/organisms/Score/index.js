import React from 'react';

import './Score.styles.scss';

const Score = ({scores}) => {
    if(scores.length === 0) {
        return <>No game played</>
    }
    return (<>
    {scores.map((games, key) => {
        return <div className="scores p1" key={key}>
        <p className="player1"><span className="p1">{games.x.name}</span> (<span className="x">x</span>)<span className="score">{games.x.win}</span></p>
        <p className="ties">Tie<span className="score">{games.games.total - games.x.win - games.o.win}</span></p>
        <p className="player2"><span className="p1">{games.o.name}</span> (<span className="o">o</span>)<span className="score appear">{games.o.win}</span></p>
        </div>

      })}
      </>
    );
}

export default Score;