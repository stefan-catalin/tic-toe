import {connect} from "react-redux";
import Game from 'modules/Game/components/pages';
import {addPlayers} from 'redux/actions/playersActions';
import {setCurrentGame} from 'redux/actions/gameActions';

const mapStateToProps = state =>{
    return {
        players : state.players.players,
        currentGame: state.game.currentGame
    };
}


const mapDispatchProps = (dispatch, props) => {
    return {
        addPlayers: players => {
            dispatch(addPlayers(players));
        },
        setCurrentGame: currentGame => {
            dispatch(setCurrentGame(currentGame));
        },
    }
}

const GamePage = connect(mapStateToProps,mapDispatchProps)(Game);


export default GamePage;