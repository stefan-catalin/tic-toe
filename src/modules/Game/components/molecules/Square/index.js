import React from 'react'

import './Square.styles.scss';
// Create Square component
 const Square = (props) => {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    )
}

export default Square;