import React, { useEffect,useState } from 'react';

import  Storage  from 'storage';

import Board from 'modules/Game/components/organisms/Board';
import Players from 'modules/Game/components/organisms/Players';
import Winner from 'modules/Game/components/organisms/Winner';

const Game = (props) => {
    const [playGame, setPlayGame] = useState(false);
    const [storage,setStorage] = useState(null);
    const [games,setGames] = useState(null);
    const [showWinner,setShowWinner] = useState(null);

    useEffect(() => {
        async function getStorageData () {
            const localStorage = new Storage();
            const data = await localStorage.getData();
            setStorage(localStorage);
            setGames(data);
        }
        getStorageData();
        
        if(props.players[0]){
            setPlayGame(true);
        }
        
    },[]);
    
    const startGame = (value) =>{
        setPlayGame(true);
        storage.update(value);
        setGames(value);
    }
    const newGame = () => {
        setShowWinner(null);

    }
    return (
        <div>
            {!props.players[0] &&  <Players startGame={value => startGame(value)} games={games} storage={storage} {...props}/>}
            {playGame && !showWinner && <Board  games={games} storage={storage} setShowWinner={setShowWinner} {...props}/>}
            {showWinner && <Winner message={showWinner} newGame={newGame}/>} 
        </div>
      );
    };

export default Game;