import React from 'react';

const Winner = ({message, newGame}) => {

    return (<>
        <p>{message}</p>
        <button onClick={newGame} >New game</button>
        </>
    )
}

export default Winner;