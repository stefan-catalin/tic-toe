import React,{useEffect, useState} from 'react'
import Square from 'modules/Game/components/molecules/Square';


import * as utils from 'utils/functions';

import './Board.styles.scss';

// Create Square component
const Board = (props) =>{

    const [boxes, setBoxes] = useState(Array(9).fill(null));
    const [xIsNext, setXIsNext] = useState(true);
    const [hasWin, setHasWin] = useState(false);
    const [status, setStatus] = useState(`It is X's turn.`);

    // Handle click on boxes on the board.
    const handleSquareClick = (index) =>{
        
        // get current state of boxes
        const auxBoxes = boxes.slice()

       // Stop the game if board contains winning combination
        if (utils.findWinner(auxBoxes) || auxBoxes[index]) {
            return
        }

        // Stop the game if all boxes are clicked (filled)
        if(utils.areAllBoxesClicked(auxBoxes) === true) {
            return
        }

        // Mark the box either as 'x' or 'o'
        auxBoxes[index] = xIsNext ? 'x' : 'o'
        
        
        // Update component state with new data
        setBoxes(auxBoxes);
        setXIsNext(!xIsNext);
 
        checkGamesStatus(auxBoxes);
    }

    const checkGamesStatus = (boxes) => {
        // Get winner (if there is any)
        const winner = utils.findWinner(boxes)

        // Are all boxes checked?
        const isFilled = utils.areAllBoxesClicked(boxes)

            // Status message
        let status = '';
        let games = props.games;
        if (winner && !hasWin) {
            // If winner exists, create status message
            status = `The winner is: ${games[props.currentGame][winner].name}!`
            
            games[props.currentGame][winner].win = games[props.currentGame][winner].win+1;
            games[props.currentGame].games.total = games[props.currentGame].games.total+1;
            setHasWin(true);
           
            // Push data about the game to storage
            props.storage.update(
                games);
            props.setShowWinner(status);
        } else if(!winner && isFilled) {
            // If game is drawn, create status message
            status = 'Game drawn!'
            
            // Push data about the game to storage
            games[props.currentGame].games.total = games[props.currentGame].games.total+1;
            
            // Push data about the game to storage
            props.storage.update(
                games);
            props.setShowWinner(status);
        } else {
            // If there is no winner and game is not drawn, ask the next player to make a move
            status = `It is ${(xIsNext ? 'O' : 'X')}'s turn.`
        }
        setStatus(status);
    }

    return (
        <>
            {/* The game board */}
            <div className="board-wrapper">
                <div className="board">
                <h2 className="board-heading">{status}</h2>
                    <div className="board-row">
                        <Square value={boxes[0]} onClick={() => handleSquareClick(0)} />

                        <Square value={boxes[1]} onClick={() => handleSquareClick(1)} />

                        <Square value={boxes[2]} onClick={() => handleSquareClick(2)} />
                    </div>

                    <div className="board-row">
                        <Square value={boxes[3]} onClick={() => handleSquareClick(3)} />

                        <Square value={boxes[4]} onClick={() => handleSquareClick(4)} />

                        <Square value={boxes[5]} onClick={() => handleSquareClick(5)} />
                    </div>

                    <div className="board-row">
                        <Square value={boxes[6]} onClick={() => handleSquareClick(6)} />

                        <Square value={boxes[7]} onClick={() => handleSquareClick(7)} />

                        <Square value={boxes[8]} onClick={() => handleSquareClick(8)} />
                    </div>
                </div>
            </div>
        </>
    );
}
export default Board;
// export default class Board extends React.Component {
//     constructor(props) {
//         super(props)
    
//             // Initialize component state
//             this.state = {
//                 boxes: Array(9).fill(null),
//                 xIsNext: true,
//                 hasWin:false,
//                 status: `It is X's turn.`
//             }

//         }

//     // Handle click on boxes on the board.
//     handleSquareClick(index) {
        
//         // get current state of boxes
//         const boxes = boxes.slice()

//        // Stop the game if board contains winning combination
//         if (utils.findWinner(boxes) || boxes[index]) {
//             return
//         }

//         // Stop the game if all boxes are clicked (filled)
//         if(utils.areAllBoxesClicked(boxes) === true) {
//             return
//         }

//         // Mark the box either as 'x' or 'o'
//         boxes[index] = xIsNext ? 'x' : 'o'
        
        
//         // Update component state with new data
//         this.setState({
//             boxes: boxes,
//             xIsNext: !xIsNext
//         })
//         this.checkGamesStatus(boxes);
//     }

//     checkGamesStatus = (boxes) => {
//         // Get winner (if there is any)
//         const winner = utils.findWinner(boxes)

//         // Are all boxes checked?
//         const isFilled = utils.areAllBoxesClicked(boxes)

//             // Status message
//         let status = '';
//         let games = props.games;
//         if (winner && !hasWin) {
//             // If winner exists, create status message
//             status = `The winner is: ${games[props.currentGame][winner].name}!`
            
//             games[props.currentGame][winner].win = games[props.currentGame][winner].win+1;
//             games[props.currentGame].games.total = games[props.currentGame].games.total+1;
//             this.setState({hasWin:true});
//             // Push data about the game to storage
//             props.storage.update(
//                 games);
//             props.setShowWinner(status);
//         } else if(!winner && isFilled) {
//             // If game is drawn, create status message
//             status = 'Game drawn!'
            
//             // Push data about the game to storage
//             games[props.currentGame].games.total = games[props.currentGame].games.total+1;
            
//             // Push data about the game to storage
//             props.storage.update(
//                 games);
//             props.setShowWinner(status);
//         } else {
//             // If there is no winner and game is not drawn, ask the next player to make a move
//             status = `It is ${(xIsNext ? 'O' : 'X')}'s turn.`
//         }
//         this.setState({status:status});
//     }
//     render() {
        
//         return (
//             <>
//                 {/* The game board */}
//                 <div className="board-wrapper">
//                     <div className="board">
//                     <h2 className="board-heading">{status}</h2>
//                         <div className="board-row">
//                             <Square value={boxes[0]} onClick={() => this.handleSquareClick(0)} />

//                             <Square value={boxes[1]} onClick={() => this.handleSquareClick(1)} />

//                             <Square value={boxes[2]} onClick={() => this.handleSquareClick(2)} />
//                         </div>

//                         <div className="board-row">
//                             <Square value={boxes[3]} onClick={() => this.handleSquareClick(3)} />

//                             <Square value={boxes[4]} onClick={() => this.handleSquareClick(4)} />

//                             <Square value={boxes[5]} onClick={() => this.handleSquareClick(5)} />
//                         </div>

//                         <div className="board-row">
//                             <Square value={boxes[6]} onClick={() => this.handleSquareClick(6)} />

//                             <Square value={boxes[7]} onClick={() => this.handleSquareClick(7)} />

//                             <Square value={boxes[8]} onClick={() => this.handleSquareClick(8)} />
//                         </div>
//                     </div>
//                 </div>
//             </>
//         );
//     }
// }