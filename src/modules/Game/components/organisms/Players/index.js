import React,{ useState} from 'react'

import './Players.styles.scss';
// Create Square component
 const Players = (props) => {

     const [player1,setPlayer1] = useState('');
     const [player2,setPlayer2] = useState('');


    const addPlayers = () =>{
        if(player1 !== '' && player2 !== ''){
            const players = {x:{name:player1,win:0},o:{name:player2,win:0},games:{total:0}};
            props.addPlayers([players]);
            props.startGame([...props.games,players]);
            props.setCurrentGame(props.games.length);
            
        }
    }
    
    return (
        <>
            <div  className="form">
                <div>
                    <label className="label-email">
                    <input type="text" className="text" value={player1} placeholder="Enter player name" tabIndex="1" required onChange={(evt) =>setPlayer1(evt.target.value)} />
                    <span className="required">Player 1</span>
                    </label>
                </div>
                <div>
                    <label className="label-email">
                    <input type="text" className="text" value={player2} placeholder="Enter player name" tabIndex="2" required onChange={(evt) =>setPlayer2(evt.target.value)}/>
                    <span className="required">Player 2</span>
                    </label>
                </div>
                <input type="submit" value="Play" onClick={addPlayers}/>


            </div>
        </>
    );
    
}

export default Players;