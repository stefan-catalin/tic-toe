import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import './App.css';
import  Results  from 'modules/Results';
import  Game  from './modules/Game';
import Layout from './layout';

function App() {
    return (
      <div className="app">
        <BrowserRouter>
          <Route exact path="/" component={Layout(Game)}/>
          <Route path="/results" component={Layout(Results)}/>
        </BrowserRouter>
      </div>
    )
}

export default App;
