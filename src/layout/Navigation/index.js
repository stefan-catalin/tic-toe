import React from 'react';
import { Link } from 'react-router-dom';

const Navigation = (active) =>{
        return (
            <nav>
                <Link to="/">Play</Link>
                <Link to="/results">Results</Link>
            </nav>
        )
}

export default Navigation;