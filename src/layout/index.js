import React from 'react';
import Navigation from './Navigation';

const Layout = (Component = 'div') =>{
    return props =>{
        return (
            <>
            <Navigation {...props}/>
            <Component {...props}/>
            </>
        )
    }
}

export default Layout;