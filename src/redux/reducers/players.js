import {ADD_PLAYERS} from 'redux/actions/playersActions';
const defaultState = {
  players:[]
};

export default function playersReducer  (state = defaultState, action) {
  switch (action.type) {
    case ADD_PLAYERS: 
      return {...state, players: action.payload.data};
    default:
      return state;
  }
}