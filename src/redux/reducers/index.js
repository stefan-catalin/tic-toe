import { combineReducers } from 'redux'

import game from './game';
import players from './players';

const rootReducer = combineReducers({
  // Define a top-level state field named `todos`, handled by `todosReducer`
  game,
  players,
})

export default rootReducer;
