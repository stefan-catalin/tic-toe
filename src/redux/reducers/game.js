 import {SET_CURRENT_GAME} from 'redux/actions/gameActions';
const defaultState = {
    currentGame: null
};

export default function gameReducer  (state = defaultState, action) {
  switch (action.type) {
    case SET_CURRENT_GAME: 
      return {...state, currentGame: action.payload.data};
    default:
      return state;
  }
}