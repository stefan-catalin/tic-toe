export const SET_CURRENT_GAME = 'SET_CURRENT_GAME';
export const SAVE_GAME = 'SAVE_GAME';

export const setCurrentGame = (currentGame) => {
    return {
        type:SET_CURRENT_GAME,
        payload:{
            data:currentGame
        }
    }
}